package be.kdg.cityhall.view;

public class Presenter {
    private final CityHallPane view;

    public Presenter(CityHallPane view) {
        this.view = view;

        this.addEventHandlers();
    }

    private void addEventHandlers() {
        // TODO: add event handlers to your controls.
    }
}
