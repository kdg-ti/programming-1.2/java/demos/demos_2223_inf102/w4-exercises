package be.kdg.time.view;

import be.kdg.time.model.TimeModel;
import java.time.Duration;
import java.time.LocalTime;

public class Presenter {
  private final int MINUTES_PER_HOUR = 60;
  private TimeModel model;
  private TimeView view;

  public Presenter(TimeModel model, TimeView view) {
    this.model = model;
    this.view = view;
    addEventHandlers();
    updateView();

  }

  private void addEventHandlers() {
    view.getSlider().setOnMouseDragged(event -> {
      double sliderValue = view.getSlider().getValue();
      //Slider theSlider = (Slider)event.getTarget();
      int hours = (int) sliderValue;
      int minutes = (int) ((sliderValue - hours) * Duration.ofHours(1).toMinutes());
      model.setCurrentTime(LocalTime.of(hours, minutes));
      updateView();
    });
  }

  private void updateView() {
    view.applyDaylightSun(model.getDaylightPercentage(),
        model.getSunHeight(),
        model.getSunPositionX());
    LocalTime modelTime = model.getCurrentTime();
    view.getSlider()
        .setValue(modelTime.getHour() +
            modelTime.getMinute() / (double) Duration.ofHours(1).toMinutes());
  }


}
