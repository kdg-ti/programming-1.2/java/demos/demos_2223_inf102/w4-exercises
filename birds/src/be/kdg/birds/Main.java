package be.kdg.birds;

import be.kdg.birds.view.BirdView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
  public void start(Stage stage) {
    BirdView view = new BirdView();
    Scene scene = new Scene(view);
    stage.setScene(scene);
    stage.setTitle("Birds");
    stage.getIcons().add(new Image("/angrybird.png"));
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
