package be.kdg.birds.view;

import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class BirdView extends BorderPane {
  private MenuBar birdBar;
  private CheckBox angryCheck;
  public BirdView() {
    initialiseNodes();
    layoutNodes();
  }
  private void initialiseNodes() {
    birdBar = new MenuBar(new Menu(
        "File",
        new ImageView("/angrybird.png"),
        new MenuItem("Bird",new ImageView("/angrybird.png"))));
    angryCheck = new CheckBox();
    angryCheck.setGraphic(new ImageView("/angrybird.png"));
// create and configure controls
// button = new Button("...")
// label = new Label("...")

  }
  private void layoutNodes() {
    setTop(birdBar);
setCenter(angryCheck);

// add/set ... methods
// Insets, padding, alignment, ...
  }
}
